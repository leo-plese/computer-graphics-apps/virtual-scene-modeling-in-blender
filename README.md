# Virtual Scene Modeling in Blender

Virtual scene models made in Blender modeling tool.

Results are listed and described in "ResultsReport.pdf".

My lab assignment in Introduction to Virtual Environments, FER, Zagreb.

Task descriptions in "TaskSpecification.pdf".

Created: 2020